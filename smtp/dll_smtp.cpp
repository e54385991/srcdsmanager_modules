// Dll1.cpp: 定义 DLL 应用程序的导出函数。
//
#include "stdafx.h"
#define DLLSMTP_EXPORTS
#include "dll_smtp.h"
#include <string>
#include <iostream>
#include "email.h"

using std::string;

int _stdcall xnet_SendMailBylibcurl(const char * SMTPServer , char * ToMail, char * SubJect,char *body,char * EmailFrom,char * username,char * password, char * cc)
{
	Email e;
	int curlError = 0;

	if(SMTPServer == nullptr)
	{
		return 0;
	}
	if (ToMail == nullptr)
	{
		return 0;
	}
	if (SubJect == nullptr)
	{
		return 0;
	}
	if (body == nullptr)
	{
		return 0;
	}
	if (EmailFrom == nullptr)
	{
		return 0;
	}
	if (username == nullptr)
	{
		e.setSMTP_username("");
	}else e.setSMTP_username(username);
	
	if (password == nullptr)
	{
		e.setSMTP_password("");
	}
	else e.setSMTP_password(password);

	if (cc == nullptr)
	{
		e.setCc("");
	}
	else e.setCc(cc);


	e.setTo(ToMail);
	e.setFrom(EmailFrom);
	e.setSubject(SubJect);

	//e.setCc("5485991@qq.com");


	e.setBody(body);
	e.setSMTP_host(SMTPServer);
	e.constructEmail();
	e.dump();


	curlError = e.send();

	if (curlError) {
		return curlError;
		//cout << "Error sending email!" << endl;
	}

	else {
		return 1;
		//cout << "Email sent successfully!" << endl;
	}
	

}
//(USERNAME, PASSWORD, SMTPSERVER, 25);