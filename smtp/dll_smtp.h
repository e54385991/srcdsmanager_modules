#ifndef _DLL_SMTP_H_
#define _DLL_SMTP_H_
#ifdef DLLSMTP_EXPORTS
#define DLL_SMTP extern "C" __declspec(dllexport) 
#else
#define DLL_SMTP extern "C" __declspec(dllimport)
#endif
DLL_SMTP int __stdcall Add(int a, int b);
#endif
